# Eggrty-Cli

## Guide

全局安装即可.

```bash
npm install -g git+https://gitlab.com/eggrty/eggrty-cli.git
```

## Getting Start

```
$ npm install typescript -g
$ npm install eggrty-cli -g
$ eggrty-cli init <Your Project Name>
$ cd <Your Project Name>
$ npm i
$ npm start
$ open http://localhost:8080
```

## 注意事项

* 迭代时要维护好 unit test
* 提交代码后要注意 CI 平台、邮件是否有单测失败反馈
